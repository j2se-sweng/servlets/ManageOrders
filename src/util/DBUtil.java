package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
	
	public static final String DRIVER = "com.mysql.jdbc.Driver";	
	public static final String URL = "jdbc:mysql://localhost:3306/" ;
	public static final String DATABASE_NAME = "ManageOrders";	

	public static Connection getConnection()
		throws ClassNotFoundException, SQLException {
		
		Class.forName(DRIVER);
	
		String databaseUser = "corso" ;
		String databasePassword = "corso" ;
		
		String databaseURL =  URL + DATABASE_NAME ;
		
		return DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
		
	}
	
	public static void closeConnection(Connection connection) throws SQLException {
		connection.close();
	}

}
