import util.DBUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class ManagerSelectOrderByCustomer {
	
	public ManagerSelectOrderByCustomer(String customerName) {
		
		_name = customerName;
	}
	
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		ManagerSelectOrderByCustomer selectCustomerByOrder = new ManagerSelectOrderByCustomer("");
		selectCustomerByOrder.doMain(args);		
	}

	protected void doMain(String[] args) throws SQLException {
		Connection connection = null;
		ResultSet resultSetOrders = null;
		
		try {
			connection = DBUtil.getConnection();
			
			Statement statement = connection.createStatement(); 	
						
			ResultSet resultSet  = statement.executeQuery(_getSelectCustomerSQL(_name));
			System.out.println("Selezione Ordine per Nome:");
			
			while (resultSet.next()) {
				long customerId = resultSet.getLong(1);
				String name = resultSet.getString(2);
				resultSetOrders = statement.executeQuery(_getSelectOrderByCustomer(customerId));
				
				System.out.println("Ordini di " + name + " (" + customerId + ") " + ":" );
				
				while (resultSetOrders.next()) {
					long orderId = resultSetOrders.getLong(1);
					String orderDate = resultSetOrders.getString(3);
					double amount = resultSetOrders.getLong(4);
					
					System.out.println(orderId + " " + customerId + " " + orderDate + " " + amount);
				}				
				
				resultSetOrders.close();
				
			}
			
			
			resultSet.close();			
			
		}
		
		catch(Exception e) {
			System.out.println(e.toString());
		}
		finally {
			if (connection != null) {
				DBUtil.closeConnection(connection);
			}
		}
		
	}	
	private String _getSelectCustomerSQL(String name) {
		
		String sql1 = "SELECT * FROM " + _CUSTOMER_TABLE ;
		
		if (name.length() != 0)
			sql1 += " WHERE name = '" + name + "'";
		
		return sql1;
		
	}
	
	private String _getSelectOrderByCustomer(long customerId) {
		
		String sql = "SELECT * FROM " + _ORDER_TABLE + " WHERE customerId = " + customerId ;
		
		return sql;
	}
			
	private	static final String	_ORDER_TABLE = "Order_";
	private static final String _CUSTOMER_TABLE = "Customer";
	
	private static String _name = null;
	
	private static Logger _log = Logger.getLogger(ManagerSelectOrderByCustomer.class.getName());
	
	
}

