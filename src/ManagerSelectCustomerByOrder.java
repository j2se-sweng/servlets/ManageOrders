import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import util.DBUtil;


public class ManagerSelectCustomerByOrder {

	public ManagerSelectCustomerByOrder() {		
		_orderId = 0;
	}
	
	public ManagerSelectCustomerByOrder(long orderId) {
		_orderId = orderId;
	}
	
	public static void main(String[] args) throws SQLException {

		ManagerSelectCustomerByOrder selectCustomerByOrder = 
				new ManagerSelectCustomerByOrder();
		selectCustomerByOrder.doMain(args);		
	}

	protected void doMain(String[] args) throws SQLException {

		System.out.println(SelectCustomerByOrder(_orderId));
	}	

	
	public String  toString(long Id) throws SQLException {
		return SelectCustomerByOrder(Id);
	}

	private String SelectCustomerByOrder(long Id) throws SQLException {
		
		Connection connection = null;
		ResultSet resultSetCustomers = null;
		String name = "";
		
		try {
			connection = DBUtil.getConnection();
			
			Statement statement = connection.createStatement(); 	
						
			ResultSet resultSet  = statement.executeQuery(_getSelectOrderSQL(_orderId));
			
			while (resultSet.next()) {
				long orderId = resultSet.getLong(1);
				long customerId = resultSet.getLong(2);
			
				System.out.println("Numero di ordine : " + orderId );
				
				Statement statement2 = connection.createStatement();
				resultSetCustomers = statement2.executeQuery(_getSelectCustomerSQL(customerId));
							
				while (resultSetCustomers.next()) {
					name = resultSetCustomers.getString(2);					
					System.out.println(orderId + " " + customerId + " " + name );
					
				}				
				
				resultSetCustomers.close();
				
			}
			
			
			resultSet.close();			
			
			
		}
		
		catch(Exception e) {
			System.out.println(e.toString());
		}
		finally {
			if (connection != null) {
				DBUtil.closeConnection(connection);
			}
		}	
		
		_name = name;
		return _name;
	}
	
	private String _getSelectOrderSQL(long orderId) {
		
		String sql1 = "SELECT * FROM " + _ORDER_TABLE + " WHERE orderId = " + 
							orderId +" ;";
					
		return sql1;
		
	}
	
	private String _getSelectCustomerSQL(long customerId) {
		
		String sql = "SELECT * FROM " + _CUSTOMER_TABLE + 
						" WHERE customerId = " + customerId + " ;";
		
		return sql;
	}
			
	private	static final String	_ORDER_TABLE = "Order_";
	private static final String _CUSTOMER_TABLE = "Customer";
	
	private long _orderId = 6;
	
	private String _name = "";
	
	private static Logger _log = Logger.getLogger(ManagerSelectCustomerByOrder.class.getName());

}
