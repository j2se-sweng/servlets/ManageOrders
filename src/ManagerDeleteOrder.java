import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import util.DBUtil;

public class ManagerDeleteOrder {

	public static void main(String[] args) throws SQLException {

		ManagerDeleteOrder DeleteOrder = new ManagerDeleteOrder();
		DeleteOrder.doMain(args);
	}

	protected void doMain(String[] args) throws SQLException {
		
		delete(_orderId);
	}
		
	public void delete(long orderId) throws SQLException {
		Connection connection = null;
		
		try {
			connection = DBUtil.getConnection();
			
			Statement statement = connection.createStatement(); 	
							
			int tableResult = statement.executeUpdate(_getDeleteOrderSQL(_orderId));
			
			_log.info("Order deletion result = " + tableResult);
			
			if(tableResult == 0)
				System.out.println("Order " + _orderId + " not found.");
		}
	
		catch(Exception e) {
				System.out.println(e.toString());
		}
		finally {
			if (connection != null) {
					DBUtil.closeConnection(connection);
			}
		}	
	}
	
	private String _getDeleteOrderSQL(long orderId) {
				
			String sql = "DELETE FROM `" + DBUtil.DATABASE_NAME + "`.`" + 
							_TABLE_NAME + "`" +
							" WHERE orderId = " + orderId;

			return sql;
	}

	

	private static final String _TABLE_NAME = "Order_"; 
	
	private long _orderId = 1 ;
	
	private static Logger _log = Logger.getLogger(ManagerDeleteOrder.class.getName());
}
