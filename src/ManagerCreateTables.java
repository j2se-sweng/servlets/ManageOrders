import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import util.DBUtil;


public class ManagerCreateTables {

	public static void main(String[] args) throws SQLException {
		
		ManagerCreateTables createTables = new ManagerCreateTables();
		
		createTables.doMain(args);		
	}
	
	protected void doMain(String[] args) throws SQLException {
		Connection connection = null;
		
		try {
			connection = DBUtil.getConnection();
			
			// passa la query al database
			Statement statement = connection.createStatement(); 	
						
			int tableResult = statement.executeUpdate(_getOrderTableSQL());
						
			_log.info("Orders Table creation results = " + tableResult );
			
			tableResult = statement.executeUpdate(_getCustomerTableSQL());
			
			_log.info("Customer Table creation results = " + tableResult );
			
		}
		catch(Exception e) {
			System.out.println(e.toString());
		}
		finally {
			if (connection != null) {
				DBUtil.closeConnection(connection);
			}
		}
	}
	
	private String _getOrderTableSQL() {
		
		String sql = "CREATE TABLE `" + DBUtil.DATABASE_NAME + "`.`" + _ORDER_TABLE_NAME + "`" +
					" (" + 
					"`orderId` BIGINT NOT NULL AUTO_INCREMENT , " +
					"`customerId` BIGINT NOT NULL, " +
					"`orderDate` TIMESTAMP NOT NULL, " +
					"`amount` DOUBLE NOT NULL, " +
					"PRIMARY KEY (`orderId`)" +
					")" ;
					
		return sql;
		
	}
	
	private String _getCustomerTableSQL() {
		
		String sql = "CREATE TABLE `" + DBUtil.DATABASE_NAME + "`.`" + _CUSTOMER_TABLE_NAME + "`" +
					" (" + 
					"`customerId` BIGINT NOT NULL AUTO_INCREMENT , " +
					"`name` VARCHAR(75) NOT NULL, " +
					"PRIMARY KEY (`customerId`)" +
					")" ;
					
		return sql;
		
	}
	
	
	private static final String _ORDER_TABLE_NAME = "Order_";
	private static final String _CUSTOMER_TABLE_NAME = "Customer";
	
	private static Logger _log = Logger.getLogger(ManagerCreateTables.class.getName());
	

}
