import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import util.DBUtil;

public class ManagerInsertOrders {

	public static void main(String[] args) throws SQLException {

		ManagerInsertOrders insertOrders = new ManagerInsertOrders();
		insertOrders.doMain(args);
	}

	protected void doMain(String[] args) throws SQLException {
		Connection connection = null;
		
		try {
			connection = DBUtil.getConnection();
			
			Statement statement = connection.createStatement(); 	
							
			int tableResult = statement.executeUpdate(_getInsertOrderSQL());
				
			_log.info("Table creation result = " + tableResult);
				
		}
	
		catch(Exception e) {
				System.out.println(e.toString());
		}
		finally {
			if (connection != null) {
					DBUtil.closeConnection(connection);
			}
		}
	}
		
	private String _getInsertOrderSQL() {
				
			String sql = "INSERT INTO `" + DBUtil.DATABASE_NAME + "`.`" + _TABLE_NAME +
						"`(customerId,orderDate,amount) VALUES " +
						"('1','2008-01-01 00:00:01','20.99')" + ", " + 
						"('1','2008-01-15 00:00:01','30.99')" + ", " +
						"('1','2008-01-30 00:00:01','40.99')" + ", " +
						"('1','2008-02-01 00:00:01','20.99')" + "; " ;
			
			return sql;
	}

	

	private static final String _TABLE_NAME = "Order_"; 
			
	private static Logger _log = Logger.getLogger(ManagerInsertOrders.class.getName());

}
