
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import util.DBUtil;

public class ManagerInsertCustomers {

	public static void main(String[] args) throws SQLException {
		ManagerInsertCustomers insertCustomers = new ManagerInsertCustomers();
		insertCustomers.doMain(args);
	}

	protected void doMain(String[] args) throws SQLException {
		Connection connection = null;
			
			try {
				connection = DBUtil.getConnection();
				
				Statement statement = connection.createStatement(); 	
								
				int tableResult = statement.executeUpdate(_getInsertCustomerSQL());
					
				_log.info("Table creation result = " + tableResult);
					
			}
		
			catch(Exception e) {
					System.out.println(e.toString());
			}
			finally {
				if (connection != null) {
						DBUtil.closeConnection(connection);
				}
			}
		}
			
	private String _getInsertCustomerSQL() {
					
			String sql = "INSERT INTO `" + DBUtil.DATABASE_NAME + "`.`" + _TABLE_NAME +
						"`(name) VALUES " +
						"('Prova Prova')" + ", " + 
						"('Prova2 Prova1')" + ", " +
						"('Prova3 Prova3')" + ", " +
						"('Prova4 Prova4')" + "; " ;
				
			return sql;
	}

		
		
		private static final String _TABLE_NAME = "Customer"; 

		private static Logger _log = Logger.getLogger(ManagerInsertCustomers.class.getName());

}


